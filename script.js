const form = document.querySelector('.form'),
    container = document.querySelector('.container'),
    list = document.createElement('ol'),
    errorWrapper = document.querySelector('.error-wrapper'),
    inp = document.querySelector('.inp');

    
    container.append(list)

form.addEventListener('submit', async (e) => {
    e.preventDefault()

    if (inp.value.endsWith('.js')) {
        try {
            await loadModule(`./${inp.value}`)
        } catch(error) {
            const err = document.createElement('span')
    
            err.classList.add('error', 'invalid-feedback', 'd-block')
            inp.classList.add('is-invalid')
    
            err.textContent = error.message
    
            errorWrapper.append(err)
        }
    } else {
        try {
            checkInpValue(inp.value)
        } catch(error) {
            const err = document.createElement('span')
    
            err.classList.add('error', 'invalid-feedback', 'd-block')
            inp.classList.add('is-invalid')
    
            err.textContent = error.message
    
            errorWrapper.append(err)
        }
    }

    inp.value = ''
})

async function loadModule(src) {
    inp.classList.remove('is-invalid')
    errorWrapper.innerHTML = ''

    const module = await import(src)

    if(typeof module === 'object') checkFirstPrototype(module.default.prototype)
}

function checkInpValue (classProto) {
    inp.classList.remove('is-invalid')
    errorWrapper.innerHTML = ''

    if(window[classProto] instanceof Object) {
        list.innerHTML = ''
        checkFirstPrototype(window[classProto].prototype)
    } else throw new Error('Введенное значение не является объектом')
}

function checkFirstPrototype(classProto) {
    const item = document.createElement('li'),
        listProperties = document.createElement('ol')

    if(classProto.constructor) {
        item.textContent = classProto.constructor.name
    }

    list.append(item)
    item.append(listProperties)

    appendProperties(listProperties, classProto)
    
    checkOthersPrototype(classProto)
}

function checkOthersPrototype(classProto) {
    let currentProto = classProto

    while(Object.getPrototypeOf(currentProto)) {
        const item = document.createElement('li'),
            listProperties = document.createElement('ol');

        currentProto = Object.getPrototypeOf(currentProto)

        if(currentProto.constructor) {
            item.textContent = currentProto.constructor.name
        }

        appendProperties(listProperties, currentProto)

        item.append(listProperties)
        list.append(item)
    }
}

function appendProperties (container, classProto) {
    const descriptors = Object.getOwnPropertyDescriptors(classProto)

    Object.entries(descriptors).map(([key, value]) => {
        const item = document.createElement('li');

        item.textContent = `Свойство: ${key}. Тип свойства: ${value.get ? typeof value.get : typeof value.value}`

        container.append(item)
    })
}